use std::collections::HashMap;

pub fn square(s: u32) -> u64 {
    if s == 0 || s > 64 {
        panic!("Square must be between 1 and 64");
    }
    let mut memo: HashMap<u32, u64> = HashMap::new();
    memo.insert(1, 1);
    for i in 2..=s {
        let prev = memo.get(&(i - 1));
        memo.insert(i, prev.unwrap() * 2);
    }
    *memo.get(&s).unwrap()
}

pub fn total() -> u64 {
    let mut sum: u64 = 0;
    for i in 1..=64 {
        sum += square(i)
    }
    sum
}
