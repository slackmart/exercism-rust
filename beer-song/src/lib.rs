pub fn verse(n: u32) -> String {
    let mut modifier = if n == 1 { "bottle" } else { "bottles" };

    let first_line = if n > 0 {
        format!(
            "{} {} of beer on the wall, {} {} of beer.\n",
            n, modifier, n, modifier
        )
    } else {
        "No more bottles of beer on the wall, no more bottles of beer.\n".to_string()
    };

    let mut second_line = String::new();

    if n == 0 {
        second_line =
            "Go to the store and buy some more, 99 bottles of beer on the wall.\n".to_string();
    } else if n == 1 {
        second_line =
            "Take it down and pass it around, no more bottles of beer on the wall.\n".to_string();
    } else {
        if n - 1 == 1 {
            modifier = "bottle";
        }
        second_line = format!(
            "Take one down and pass it around, {} {} of beer on the wall.\n",
            n - 1,
            modifier
        );
    }

    first_line + &second_line
}

pub fn sing(start: u32, end: u32) -> String {
    let mut verses: Vec<String> = vec![];
    for x in (end..=start).rev() {
        verses.push(verse(x));
    }

    verses.join("\n")
}
