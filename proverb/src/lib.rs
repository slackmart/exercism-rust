pub fn build_proverb(list: &[&str]) -> String {
    let mut result = String::new();

    if list.is_empty() {
        return result;
    }

    for (i, x) in list.iter().enumerate() {
        if i + 1 == list.len() {
            break;
        }
        result = result + &format!("For want of a {} the {} was lost.\n", x, list[i + 1]);
    }
    result = result + &format!("And all for the want of a {}.", list[0]);

    result
}
