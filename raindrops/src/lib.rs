pub fn raindrops(n: u32) -> String {
    let mut answer = String::new();
    let div_by = |x| n % x == 0;
    if div_by(3) {
        answer += "Pling";
    }
    if div_by(5) {
        answer += "Plang";
    }
    if div_by(7) {
        answer += "Plong";
    }
    if answer.is_empty() {
        answer = n.to_string();
    }
    return answer;
}
