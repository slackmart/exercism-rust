fn is_prime(n: u32) -> bool {
    if n < 2 {
        return false;
    }
    let mut number = 2;
    while number * number <= n {
        if n % number == 0 {
            return false;
        }
        number += 1;
    }
    return true;
}

pub fn nth(n: u32) -> u32 {
    let mut counter = 0;
    let mut i : u32 = 2;
    let mut nth_prime = 2;

    loop {
        if counter == n {
            break;
        }
        i += 1;
        if is_prime(i) {
            counter += 1;
            nth_prime = i;
        }
    }
    return nth_prime;
}
