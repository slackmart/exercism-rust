pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    if limit <= 1 {
        return 0;
    }

    let mut multiples: Vec<u32> = vec![];
    for number in 1..limit {
        for factor in factors {
            if *factor as i32 == 0 {
                continue;
            }
            if number % factor == 0 && !multiples.contains(&number) {
                multiples.push(number);
            }
        }
    }
    multiples.iter().sum()
}
